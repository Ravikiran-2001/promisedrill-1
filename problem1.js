function racePromise1() {
  //creating function and returning promise
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("race will start at 9am"); //promise is resolved
    }, 2000);
  });
}

function racePromise2() {
  //creating function and returning promise
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("race will end at 12pm "); //promise is resolved
    }, 3000);
  });
}

function racePromise() {
  //creating function
  Promise.race([racePromise1(), racePromise2()]) //using race which will return fastest resolved promise
    .then((value) => {
      console.log(value);
    })
    .catch((error) => {
      //if error occurs in code it will redirected to catch
      console.log(error);
    });
}
racePromise(); //calling function
