const promise1 = Promise.resolve("--promise1"); //creating promise
const promise2 = Promise.resolve("--promise2"); //creating promise
const promise3 = Promise.resolve("--promise3"); //creating promise
const promise4 = Promise.resolve("--promise4"); //creating promise
const promise5 = Promise.resolve("--promise5"); //creating promise
const promise6 = Promise.resolve("--promise6"); //creating promise

//stored all promises inside array
const arrayOfPromises = [
  promise1,
  promise2,
  promise3,
  promise4,
  promise5,
  promise6,
];

function parallelLimit(arrayOfPromises, limit) {
  //creating function
  let result = []; //created empty array
  let index = 0; //created index variable and assigned with 0
  function helper() {
    //created an helper function
    const subArray = arrayOfPromises.slice(index, index + limit); //created array and stored sliced value of arrayPromises
    index += limit; //updating index
    return Promise.all(subArray).then((value) => {
      result.push(value); //pushing resolved value to result array
      if (index < arrayOfPromises.length) {
        return helper(); //if index value is less than array length again calling helper function
      }
      return result.flat(); //all method returns data in array and array is pushed inside result so my array become 2d by using flat converting it into 1d again
    });
  }
  return helper(); //returning function
}
parallelLimit(arrayOfPromises, 2)
  .then((value) => {
    console.log(value); //printing array
  })
  .catch((error) => {
    console.log(error);
  });
