function promise1() {
  //creating function which will return promise
  return new Promise((resolve, reject) => {
    resolve("promise1");
  });
}
function promise2() {
  //creating function which will return promise
  return new Promise((resolve, reject) => {
    resolve("promise2");
  });
}
function promise3() {
  //creating function which will return promise
  return new Promise((resolve, reject) => {
    resolve("promise3");
  });
}
const arrayOfPromises = [promise1(), promise2(), promise3()]; //storing promise functions inside array

function dynamicChain(arrayOfPromises) {
  //creating function
  if (Array.isArray(arrayOfPromises) === false) {
    //if input parameter is not array
    return new Promise((resolve, reject) => {
      //promise will be rejected
      reject("invalid array");
    });
  }
  let dynamicallyChain = arrayOfPromises[0]; //first function is stored in  dynamicallyChain
  for (let index = 1; index < arrayOfPromises.length; index++) {
    dynamicallyChain = dynamicallyChain.then(() => arrayOfPromises[index]); //then repeatedly chaining with next function
  }
  return dynamicallyChain; //returning value which is resolved by last promise
}
dynamicChain(arrayOfPromises)
  .then((result) => console.log(result)) //printing value
  .catch((error) => {
    console.log(error); //if error occurs then printing type of error
  });
