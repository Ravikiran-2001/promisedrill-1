const promise1 = new Promise((resolve, reject) => {
  //creating promise
  resolve("promise1");
});
const promise2 = Promise.resolve("promise2"); //creating promise
const promise3 = Promise.reject("rejected promise3"); //creating promise

const arrayOfPromises = [promise1, promise2, promise3]; //storing all promises inside array

function composePromises(arrayOfPromises) {
  //creating function and arrayOfPromises given as input parameter
  return Promise.allSettled(arrayOfPromises).then((settledPromises) => {
    //used allSettled in array which will return all promises irrespective of whether promise is resolved or rejected
    return settledPromises;
  });
}
composePromises(arrayOfPromises)
  .then((value) => {
    console.log(value); //if error does'nt occur in above code it will print values
  })
  .catch((error) => {
    console.log(error);
  });
